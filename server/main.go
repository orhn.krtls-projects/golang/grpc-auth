package main

import (
	"context"
	api "grpc-Auth/api"
	"log"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	port = ":6000"
)

type server struct {
	api.UnimplementedAuthServiceServer
}

func (s *server) IsAuthenticatedUser(ctx context.Context, request *api.UserAuthRequest) (*api.UserAuthResponse, error) {
	email := request.GetEmail()
	password := request.GetPassword()

	var authenticatedUser bool
	if email == "a@a.com" && password == "123" {
		authenticatedUser = true
	} else {
		authenticatedUser = false
	}

	return &api.UserAuthResponse{Authenticated: authenticatedUser}, nil
}

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("Failed to listen %v", err)
	}

	srv := grpc.NewServer()
	api.RegisterAuthServiceServer(srv, &server{})
	reflection.Register(srv)

	if err := srv.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	} else {
		log.Printf("server listening")
	}
}
