package main

import (
	"context"
	"grpc-Auth/api"
	"log"
	"time"

	"google.golang.org/grpc"
)

const (
	address = "localhost:6000"
)

func main() {
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()
	client := api.NewAuthServiceClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	result, err := client.IsAuthenticatedUser(ctx, &api.UserAuthRequest{Email: "a@a.com", Password: "123"})
	if err != nil {
		log.Printf("AuthenticatedUser cagirmada hata: %s", err)
	}

	log.Printf("Result: %v", result.String())
}
